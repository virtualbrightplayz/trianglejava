package net.virtualbrightplayz.triangle;

import java.util.Scanner;


public class Main {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.print("Filler: ");
    String cha = (scan.nextLine());
    System.out.print("Number: ");
    Integer inputint = Integer.parseInt(scan.nextLine());
    for (int i = 0; i < inputint; i++) {
      for (int j = inputint; j >= i; j--) {
        System.out.print(" ");
      }
      System.out.print("/");
      for (int j = 0; j < i * 2; j++) {
        System.out.print(cha.charAt(0));
      }
      System.out.println("\\");
    }
    scan.close();
  }
}